from auditorium import Show
from pathlib import Path 
from types import SimpleNamespace 
import pandas as pd

show = Show("Undian HKBP")

df = pd.read_pickle('./data.pkl')
df.fillna('0', inplace=True)

def find_range(s: str, type_a='A'):
    print(s)
    if isinstance(s, float) or s == '0': 
        return ''
    x,y = [t.strip() for t in s.split('-')]
    kupon_type=f'{type_a}00'
    x,y = x.replace(kupon_type, ''), y.replace(kupon_type,'')
    kupon_type=f'{type_a}OO'
    x,y = x.replace(kupon_type, ''), y.replace(kupon_type,'')
    return [a for a in range(int(x), int(y)+1)]

def found_range(x_range: list, type_: str="A"): 
    return [f'{type_}{str(x).zfill(5)}' for x in x_range]

def c_to_r(code, type_a: str = 'A'): 
    kupon_type=f'{type_a}00'
    x= x.replace(kupon_type, '')
    kupon_type=f'{type_a}OO'
    x= x.replace(kupon_type, '')
    return int(x)

def r_to_c(r: int, type_a: str = 'A'): 
    return f'{type_a}{str(r).zfill(5)}'


def search(code: str, jenis_kupon: str='kupon_10k'): 
    result = SimpleNamespace(nama='', remarks='0', referral='')
    if code == '-': 
        return result
    search_space = 'kupon_25k_range' if jenis_kupon == 'kupon_25k' else 'kupon_10k_range' 
    for i, item in enumerate(df[search_space]): 
        if code in item: 
            result = df.loc[i].to_dict()
            result = SimpleNamespace(**result)
    print(result)
    if result.remarks != '0': 
        result.nama = f'{result.nama}*'

    return result 


@show.slide
def gambar_10k(ctx): 
    png = Path('./hadiah_10k.png')
    # ctx.markdown(f'''<img src="/hadiah_10k.png">''')
    ctx.markdown(f'''![](https://i.postimg.cc/C1kM7Qnq/hadiah-10k.png)''')

@show.slide
def slide(ctx): 
    ctx.markdown("Nomor Kupon 10k")
    name = ctx.text_input(default="-")
    result = search(name)
    ctx.markdown(f'''# {result.nama}\n''')
    ctx.markdown(f''' referral: *{result.referral}* ''')

@show.slide
def gambar_25k(ctx): 
    png = Path('./hadiah_25k.png')
    ctx.markdown(f'''<img src="https://i.postimg.cc/hgmJB3N1/hadiah-25k.png">''')

@show.slide
def slide_25k(ctx): 
    ctx.markdown("Nomor Kupon 25k")
    name = ctx.text_input(default="-")
    result = search(name, jenis_kupon='kupon_25k')
    ctx.markdown(f'''# {result.nama}\n''')
    ctx.markdown(f''' referral: *{result.referral}* ''')

@show.slide
def fam_pic(ctx): 
    ctx.markdown('# Pemenang Family Picture')

@show.slide
def harapan(ctx): 
    ctx.markdown('## Juara Harapan')
    ctx.markdown(
    ''' 
    I. Mondang Sianturi/Ny Damanik, score **1740**


    II. Ny St Pardosi, score **1200**


    III. Lisbeth Tobing/Ny Sidabutar, score **1140**

    IV. Rita Malau, score **1140**''')

@show.slide
def pemenang(ctx): 
    ctx.markdown('## Juara')
    ctx.markdown('''
    Juara 1 : Tiurida, score **3010**

    Juara 2 : Ny. St Malau, score **2320**

    Juara 3 : Elizabeth Marbun/mama Bosco, score **2280**''')

